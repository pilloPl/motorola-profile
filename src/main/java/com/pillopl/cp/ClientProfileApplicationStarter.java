package com.pillopl.cp;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


//java 1.8 - celowo
//Spring Cloud Finchley - niecelowo ;)

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@EnableCircuitBreaker
@EnableHystrixDashboard
public class ClientProfileApplicationStarter {

    @Bean
    IRule loadBallancer() {
        return new RandomRule();
    }

    public static void main(String[] args) {
        SpringApplication.run(ClientProfileApplicationStarter.class, args);
    }


}

@FeignClient("risk")
interface RiskClient {

    @RequestMapping(method = RequestMethod.GET, value = "/evaluations")
    String evaluate();
}


@Service
class FailingRiskingClient {

    @HystrixCommand(fallbackMethod = "manual")
    String evaluate() {
        try {
            TimeUnit.MILLISECONDS.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (new Random().nextBoolean()) {
            throw new IllegalArgumentException();
        }

        if (new Random().nextBoolean()) {
            throw new IllegalArgumentException();
        }

        return "OK";
    }

    String manual() {
        return "MANUAL";
    }


}

@RestController
class TestController {

    private final RiskClient riskClient;

    TestController(RiskClient riskClient) {
        this.riskClient = riskClient;
    }

    @GetMapping("/test")
    String test() {
        return riskClient.evaluate();
    }
}


@RestController
class OfferController {

    private final OfferRepository offerRepository;
    private final OfferRepositoryV2 offerRepositoryV2;


    OfferController(OfferRepository offerRepository, OfferRepositoryV2 offerRepositoryV2) {
        this.offerRepository = offerRepository;
        this.offerRepositoryV2 = offerRepositoryV2;
    }

    @GetMapping(value = "/offers", produces = "application/vnd.card+json")
    ResponseEntity listOffers() {
        Collection<Offer> offers = offerRepository.all();

        if (offers.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity
                .ok()
                .eTag(String.valueOf(offers.hashCode()))
                .cacheControl(CacheControl.noCache())
                .body(offers);

    }

    @GetMapping(value = "/offers", produces = "application/vnd.cardV2+json")
    ResponseEntity listOffersV2() {
        Collection<OfferV2> offers = offerRepositoryV2.all();

        if (offers.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity
                .ok()
                .eTag(String.valueOf(offers.hashCode()))
                .cacheControl(CacheControl.noCache())
                .body(offers);

    }

}


@RestController
class CreditCardApplicationController {

    private final CardApplicationRepository cardApplicationRepository;

    CreditCardApplicationController(CardApplicationRepository cardApplicationRepository) {
        this.cardApplicationRepository = cardApplicationRepository;
    }


    @PostMapping("/applications")
    ResponseEntity applyForCard(@RequestBody ApplyForCardCommand cmd) {

        int id = new Random().nextInt(100);
        CardApplication app = new CardApplication(id, cmd.pesel, cmd.offerId, "");
        cardApplicationRepository.save(app);

        return ResponseEntity
                .created(
                        ServletUriComponentsBuilder
                                .fromCurrentRequest()
                                .replacePath("/applications/{id}")
                                .buildAndExpand(id)
                                .toUri()
                ).build();
    }


    @PutMapping("/applications/{id}")
    ResponseEntity addScan(@RequestBody AddScanCommand cmd, @PathVariable Integer id) {

        CardApplication app = cardApplicationRepository.byId(id);
        if (app == null) {
            return ResponseEntity.notFound().build();
        }

        app.scan = cmd.scan;

        return ResponseEntity
                .ok()
                .build();
    }

    @GetMapping("/applications/{id}")
    ResponseEntity application(@PathVariable Integer id) {
        CardApplication app = cardApplicationRepository.byId(id);
        if (app == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(new CardApplicationHateoas(app));
    }


}

class CardApplicationHateoas extends ResourceSupport {

    @Getter
    private final CardApplication cardApplication;

    CardApplicationHateoas(CardApplication cardApplication) {
        this.cardApplication = cardApplication;
        if (cardApplication.scan.isEmpty()) {
            add(linkTo(methodOn(CreditCardApplicationController.class)
                    .addScan(new AddScanCommand("scan"), cardApplication.id)).withRel("add scan"));
        }
    }
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class ApplyForCardCommand {
    String pesel;
    int offerId;

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class AddScanCommand {
    String scan;

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class CardApplication {
    int id;
    String pesel;
    int offerId;
    String scan;

}


@Data
@NoArgsConstructor
@AllArgsConstructor
class Offer {
    int id;
    int fee;
    int days;
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class OfferV2 {
    int id;
    int fee;
    String desc;
}


@Repository
class CardApplicationRepository {


    private final Map<Integer, CardApplication> offers = new HashMap<>();

    CardApplication byId(Integer id) {
        return offers.get(id);
    }

    void save(CardApplication offer) {
        offers.put(offer.id, offer);
    }

    Collection<CardApplication> all() {
        return offers.values();
    }


}

@Repository
class OfferRepository {


    private final Map<Integer, Offer> offers = new HashMap<>();

    Offer byId(Integer id) {
        return offers.get(id);
    }

    void save(Offer offer) {
        offers.put(offer.id, offer);
    }

    Collection<Offer> all() {
        return offers.values();
    }


}

@Repository
class OfferRepositoryV2 {


    private final Map<Integer, OfferV2> offers = new HashMap<>();

    OfferV2 byId(Integer id) {
        return offers.get(id);
    }

    void save(OfferV2 offer) {
        offers.put(offer.id, offer);
    }

    Collection<OfferV2> all() {
        return offers.values();
    }


}


//
//# Big Picture Event Storming
//
//        - szybki
//        - prosty
//        - stopniowy
//        - uwidacznia braki
//        - synchronizuje wiedzę
//
//
//
//
//// 1. WILD EXPLORATION (przyklejaj karteczki)
//// 2. ENFORCE TIMELINE (oś czaasu)
//        (swimlines, hotspots)
//// 3. REVERSE NARRATION (przejdz od tylu i zobacz czego brakuje)


//Klient stworzyl konto w systemie
//Zdefiniowano oferte
//Usunieto oferte
//Klient wybral oferte
//Klient zaaplikowal o karte
//Klient wprowadzil swoj numer konta
//Zdefiniowane zasady ryzyka
//Sprawdzono zasady ryzyka
//        Aktywowano karte
//        Odrzucono wniosek
//Zasady ryzyka zdefiniowany limit karty
//
//Wyslano email
//Odebrano telefon
//Odebrano email
//
//Odebrano skan dowodu
//Zweryfikowano skan dowodu
//Przeslano skan dowodu
//
//Zaaplikowano o wyplate
//Sprawdzono limit
//PRzelano pieniadze
//Naliczono odsetki
//Naliczono prowizje
//
//
//Wygenerowano wyciag
//Wyslano wyciag
//Otrzymano przelew splacajacy
//Zweryfikowano mozliwosxc uzycia karty w przyszlym miesiacu
//Deaktywwano karte


//class Book {
//
//    enum Status {ON_THE_SHELF, SOLD, PENDING, CLAIM}
//    Status status;
//    String isbn;
//    String title;
//    List<Author>;
//    BigDecimal price;
//    List<Tag> tags;
//    String publisher;
//    Instant publishedAt;
//    Type type;
//    int shelf;
//    int storageUnit;
//    List<String> photos;
//
//    void changePrice(BigDecimal newPrice) {
//        if(status == SOLD) {
//            throw new IllegalStateException();
//        }
//        if(status == CLAIM) {
//            throw new IllegalStateException();
//        }
//        price = newPrice;
//    }
//
//}
//

























